import React, { Component } from 'react';
import Header from '../Components/Header';
import ListView from '../Components/ListView';
import '../Styles/Homepage.css';
import {imageArray} from '../Json/index';


class HomePage extends Component {
  render() {
    return (
      <div>
        <Header/>
        <div className="container">
            <ListView
                news={imageArray}
                handleBookmark={(id)=>{this.props.handleBookmark(id)}}
            />
        </div>
      </div>
    );
  }
}

export default HomePage;

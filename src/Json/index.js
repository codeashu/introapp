export const imageArray = [
    {
        imageUrl:require('../assets/images/1.jpeg'),
        imageTitle:'Odisha MLA manhandles CRPF martyrs relative',
        read:'read more at The New Indian Express',
        des:'A video has surfaced online, showing BJD MLA Debashish Samantaray in Odisha manhandling a relative of CRPF jawan martyred in Pulwama terror attack during cremation. The MLA could be seen pushing the relative to the ground, forcing him to kneel before the martyrs coffin. BJP leaders were seen burning Samantarays effigies in some parts of the state.'
    },
    {
        imageUrl:require('../assets/images/2.jpg'),
        imageTitle:'Absolutely crystal, clear: Afridi on Pak PMs reaction to J&K attack',
        read:'read more at CricTracker',
        des:'Reacting to Pakistan PM Imran Khans statement on Pulwama attack, ex-Pakistan captain Shahid Afridi tweeted, "Absolutely crystal and clear." "If India retaliates, we wont think and retaliate back. If you (Indian government) think you will attack us and we will not think of retaliating...we will retaliate," Imran Khan had said. As many 40 CRPF jawans.'
    },
    {
        imageUrl:require('../assets/images/3.jpg'),
        imageTitle:'Pak PM didnt even offer lip sympathy to martyrs families: Jaitley',
        read:'read more at PIB',
        des:'Finance Minister Arun Jaitley on Tuesday said "there has been a universal condemnation of gruesome act of terror in Pulwama, but Pakistan PM Imran Khan didnt even offer lip sympathy" to the families of 40 CRPF martyrs. Jaitley further condemned Khans demand for proof of Pakistans involvement, adding the evidence is in your own home as Jaish-e-Mohammed.'
    },
    {
        imageUrl:require('../assets/images/4.jpg'),
        imageTitle:'5 batsmen score 0 as Scotland dismiss Oman for 24 in 50-over match',
        read:'read more at ICC',
        des:'Five batsmen were dismissed for ducks as Oman got all out for 24 runs in 17.1 overs against Scotland in a 50-over (List A) match on Tuesday. Khawar Ali top-scored for Oman with 15(33), while two Scotland bowlers picked up four wickets each. Scotland chased down the target without losing a wicket and with 280 balls to spare.'
    },
    {
        imageUrl:require('../assets/images/5.jpg'),
        imageTitle:'Novak Djokovic, Simone Biles win top Laureus awards',
        read:'read more at NCC',
        des:'Fifteen-time Grand Slam champion Novak Djokovic and Olympic champion Simone Biles won the Laureus Sportsman and Sportswoman of the Year awards respectively on Monday. Novak Djokovic won three straight Grand Slam titles, while Biles became all-time leading world gymnastics championship gold medal winner with a record 14 golds. FIFA World Cup 2018 champions.'
    },
    {
        imageUrl:require('../assets/images/6.jpeg'),
        imageTitle:'Jharkhand-based NGO wins Laureus Sport for Good honour',
        read:'read more at India.com',
        des:'Yuwa, a Jharkhand-based NGO which works for girl empowerment, won Laureus Sport for Good award, becoming the third Indian entry to bag the honour. The NGO, co-founded by American Franz Gastler in 2009, runs a football program covering 450 girls in rural Jharkhand. Four girls out of the 450 received the award on behalf of Yuwa.'
    },
    {
        imageUrl:require('../assets/images/7.jpeg'),
        imageTitle:'Chennai man orders food, Swiggy shows delivery man in Rajasthan',
        read:"read more at Swiggy",
        des:'A Chennai man who ordered food through Swiggys app was shown that the delivery man is leaving from Rajasthan and the food will be delivered within 12 minutes. The man tweeted, "Wow Swiggy what are you driving?" Responding to the man, Swiggy said, "We are taking the issue very seriously and are actively working on to avoid such mishaps."'
    },
    {
        imageUrl:require('../assets/images/8.jpg'),
        imageTitle:'PM Modi breaks protocol to personally receive Saudi Crown Prince',
        read:'read more at Hindustan Times',
        des:'Prime Minister Narendra Modi on Tuesday broke protocol to personally receive Saudi Arabias Crown Prince Mohammed bin Salman as he arrived in New Delhi on his first bilateral visit to India. The Crown Prince arrived in India after visiting Pakistan and returning to Riyadh. PM and Salman are expected to hold talks on defence ties and a joint naval exercise.'
    },
    {
        imageUrl:require('../assets/images/9.jpeg'),
        imageTitle:'ICJ refuses Pakistans request to adjourn Kulbhushan Jadhav case',
        read:'read more at Hindustan Times',
        des:'The International Court of Justice (ICJ) on Tuesday refused to entertain Pakistas request to adjourn the hearing in the case of Kulbhushan Jadhav. Pakistan, which was presenting its case, asked the judge to adjourn the case citing the illness of its ad hoc judge. Pakistans ad hoc judge Tassaduq Hussain Jillanis health reportedly declined on Monday.'
    }
]
import { createStore, applyMiddleware } from "redux";
import combineReducers from "./reducer";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./saga";

const middleware = [];
const sagaMiddleware = createSagaMiddleware();

middleware.push(sagaMiddleware);


export const store = createStore(combineReducers, applyMiddleware(...middleware));

sagaMiddleware.run(rootSaga);
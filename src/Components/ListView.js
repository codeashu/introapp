import React, { Component } from 'react';
import '../Styles/Listview.css';
import 'font-awesome/css/font-awesome.min.css';


class Header extends Component {
  render() {
    const { news } = this.props;
    let list = news.map((val,i) => {
        return(
            <div className="listview">
                <img src={val.imageUrl} className="image" />    
                <div className="content">
                    <div className="tittle-icon">
                        <span className="heading">
                            {val.imageTitle}
                        </span>
                        <div onClick={()=>{this.props.handleBookmark(val.id)}} className="m-t">
                            <i className="fa fa-bookmark icon-size"></i>
                        </div>
                    </div>
                    <span className="Description">
                        {val.des}
                    </span>
                    <div className="dislike">
                        <span className="readMore">
                            {val.read}
                        </span>
                        <div onClick={()=>{this.props.handleLike(val.id)}}>
                            <i className="fa fa-thumbs-up icon-size">1</i>
                        </div>
                        <div onClick={()=>{this.props.handleDislike(val.id)}}>
                            <i className="fa fa-thumbs-down icon-size">2</i>
                        </div>
                    </div>
                </div>
            </div>
        )
    });
    return (
        <div>
            {list}
        </div>
    );
  }
}

export default Header;

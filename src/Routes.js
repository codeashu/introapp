import React, { Component } from 'react';
import App from './App';
import HomePage from './Container/Homepage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

class Routes extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
            <Switch>
                <Route exact path="/" component={HomePage}/>
            </Switch>
        </React.Fragment>
      </Router>
    );
  }
}

export default Routes;
